;;;; utils-git.import.scm - GENERATED BY CHICKEN 5.3.0 -*- Scheme -*-

(##sys#with-environment
  (lambda ()
    (##sys#register-compiled-module
      'utils-git
      'utils-git
      (scheme#list)
      '((git-copy . utils-git#git-copy)
        (git-contributors . utils-git#git-contributors)
        (git-commits . utils-git#git-commits)
        (git-config->string . utils-git#git-config->string)
        (git-file->lines . utils-git#git-file->lines)
        (git-repository->paths-list . utils-git#git-repository->paths-list)
        (git-file-is-text? . utils-git#git-file-is-text?)
        (in-git-directory? . utils-git#in-git-directory?)
        (lines-from-git-command . utils-git#lines-from-git-command))
      (scheme#list)
      (scheme#list))))

;; END OF FILE
