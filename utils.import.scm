;;;; utils.import.scm - GENERATED BY CHICKEN 5.3.0 -*- Scheme -*-

(##sys#with-environment
  (lambda ()
    (##sys#register-compiled-module
      'utils
      'utils
      (scheme#list)
      '((unless-equals . utils#unless-equals)
        (alist-update-in . utils#alist-update-in)
        (alist-ref-in . utils#alist-ref-in)
        (alist-merge . utils#alist-merge)
        (substring* . utils#substring*)
        (pathparts . utils#pathparts)
        (inspect . utils#inspect)
        (bail . utils#bail))
      (scheme#list)
      (scheme#list))))

;; END OF FILE
