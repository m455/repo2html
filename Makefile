DEPS = utf8 lowdown sxml-transforms clojurian symbol-utils scss srfi-1 srfi-13 srfi-14 ersatz
SRC = main.scm
LOCAL_MODULES = utils utils-git
DIR_INSTALL ?= /usr/local/bin
BIN = repo2html

.PHONY: clean clean-after-compile uninstall install dependencies

default: $(BIN) clean-after-compile

%: %.scm
	csc -O3 -static -strip $<

# Note: things break if you use -O5 here.
%.import.scm: %.o
	# $@ will have been created by the command to build $<
%.o: %.scm
	csc -O3 -static -c -unit $* -emit-import-library $* $<

$(DIR_INSTALL)/%: %
	install -Dm755 $< -D $@

clean-after-compile: $(BIN)
	rm -f $<.link
	@ls -sh $<
	@echo "Finished compiling static binary $<!"

repo2html: utils.import.scm utils.o utils-git.import.scm utils-git.o

dependencies:
	chicken-install $(DEPS)

install: $(DIR_INSTALL)/$(BIN)

uninstall:
	rm -f $(DIR_INSTALL)/$(BIN)

clean:
	rm -f $(BIN) foo repo2html *.link *.o *.import.scm

compile-on-debian-11:
	# builds repo2html compatible with systems running glibc-2.31 and higher.
	docker build docker -t repo2htmlbuilder
	# the docker image "repo2htmlbuilder:latest" now exists on your system.
	# this will make the next time you run this command go faster.
	# to delete it and reclaim the space run "docker image rm repo2htmlbuilder:latest"
	docker run -v $(shell pwd):/tmp/x -w /tmp/x --rm repo2htmlbuilder make
