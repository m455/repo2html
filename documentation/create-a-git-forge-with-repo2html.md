# Create a Git forge with repo2html

This tutorial teaches you how to use `repo2html` in a `post-receive` hook to
auto-generate HTML representations of bare Git repositories on a remote web
server after you `git push` to them.

A Git forge is a website that provides HTML representations of Git
repositories, so visitors don't need to clone repositories to view their
contents.

## Page contents

<!-- vim-markdown-toc GFM -->

- [Requirements](#requirements)
- [Prepare your server](#prepare-your-server)
    - [Set up a git user](#set-up-a-git-user)
    - [Set up nginx](#set-up-nginx)
    - [Install repo2html](#install-repo2html)
    - [Set up a post-receive hook](#set-up-a-post-receive-hook)
    - [Enable cloning over git://](#enable-cloning-over-git)
- [Test your post-receive hook locally](#test-your-post-receive-hook-locally)

<!-- vim-markdown-toc -->

## Requirements

- [Chicken Scheme](https://call-cc.org/), and eggs:
    - [clojurian](https://wiki.call-cc.org/eggref/5/clojurian)
    - [ersatz](https://wiki.call-cc.org/eggref/5/ersatz)
    - [lowdown](https://wiki.call-cc.org/eggref/5/lowdown)
    - [scss](https://wiki.call-cc.org/eggref/5/scss)
    - [srfi-1](https://wiki.call-cc.org/eggref/5/srfi-1)
    - [srfi-13](https://wiki.call-cc.org/eggref/5/srfi-13)
    - [srfi-14](https://wiki.call-cc.org/eggref/5/srfi-14)
    - [sxml-transforms](https://wiki.call-cc.org/eggref/5/sxml-transforms)
    - [symbol-utils](https://wiki.call-cc.org/eggref/5/symbol-utils)
    - [utf8](https://wiki.call-cc.org/eggref/5/utf8)
- Git
- nginx

## Prepare your server

This section uses `example.com` as a placeholder value. Ensure you replace
`example.com` with your own domain when following the procedures below.

This section assumes the following about your server:

- You've generated public and private SSH keys on your local machine.
- You can access your server through SSH and have root access to your server.
- You manage your firewall with `ufw`.
- You use `nginx` as your web server.
- You use letsencrypt to manage TLS certificates.
- You've added an A record for `git.example.com`.

### Set up a git user

Ensure you're in the repo2html git repository, and follow the steps below:

1. As root, run `adduser git`.
2. As root, run `mkdir /var/www/git && chown git:git /var/www/git`.
3. As root, run `ufw allow 9418`.
4. Run `su git`.
5. Run `mkdir ~/.ssh && chmod 700 ~/.ssh`.
6. Run `touch ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys`.
7. Add your public ssh key from your local machine to `~/.ssh/authorized_keys`.
8. Run `mkdir ~/projects`.
9. Run `git init --bare my-repository`.

After you've set up a git user, follow all procedures that don't require root
as the git user.

### Set up nginx

1. As root, add the following contents to `/etc/nginx/sites-available/git.example.com`:

        server {
            root /var/www/git;
            index index.html;
            server_name git.example.com;
        }

2. As root, run `ln -s /etc/nginx/sites-available/git.example.com /etc/nginx/sites-enabled/`.
3. As root, run `nginx -t` to test your nginx configuration.
4. As root, run `certbot`, and follow the prompts.
5. As root, run `systemctl restart nginx`.

### Install repo2html

Ensure you're in the repo2html git repository, and follow the steps below:

1. As root, run `make dependencies`.
2. Run `make`.
3. As root, run `make install`.

### Set up a post-receive hook

1. Ensure you're in the `repo2html` Git repository.
2. Run `mkdir ~/bin`.
3. Run `echo "PATH="~/bin:$PATH" >> ~/.profile`.
4. Run `cp assets/post-receive ~/bin`.
5. Run `chmod u+x ~/bin/post-receive`.
6. Run `ln -sf ~/bin/post-receive ~/projects/my-repository/hooks/post-receive`.
7. Run `cp assets/templates/default.html ~/bin`.
8. In the `assets/post-receive` file, change
   `path/to/directory/containing/template` to `~/bin`

### Enable cloning over git://

1. Ensure you're in the `repo2html` Git repository.
2. As root, run `cp assets/git-daemon.service /etc/systemd/system`.
3. As root, run `systemctl enable --now git-daemon.service`.

## Test your post-receive hook locally

This section uses `example.com` as a placeholder value. Ensure you replace
`example.com` with your own domain when following the procedures below.

On your local machine, follow the steps below:

1. Run `git init my-repository`.
2. Run `cd my-repository`.
3. Run `echo "hello" > my-file.txt`.
4. Run `git add my-file.txt`.
5. Run `git commit -m "my first commit"`.
6. Run `git remote add origin git@example.com:~/projects/my-repository`.
7. Run `git push`.
8. Navigate to `https://git.example.com/my-repository` in your browser.




