# repo2html

A command-line tool that generates a static HTML representation of a Git
repository.

## Page contents

<!-- vim-markdown-toc GFM -->

- [Features](#features)
- [Requirements](#requirements)
- [Usage](#usage)
- [Installation](#installation)
- [Configuration](#configuration)
    - [Templates](#templates)
    - [Environment variables](#environment-variables)
- [Creating a Git forge on your web server](#creating-a-git-forge-on-your-web-server)
- [Alternative Git-to-HTML tools](#alternative-git-to-html-tools)
- [Existing Git forges](#existing-git-forges)
- [Existing Git forge software](#existing-git-forge-software)
- [Todos](#todos)

<!-- vim-markdown-toc -->

## Features

- Static html files
- Customizable templates
- Can be used as a standalone command-line tool, or in a Git hook
- Built-in, plaintext issue support
- Image support
- Markdown files are rendered as HTML
- No resident background process

## Requirements

- [Chicken Scheme](https://call-cc.org/)
- The following Chicken Scheme eggs:
    - [clojurian](https://wiki.call-cc.org/eggref/5/clojurian)
    - [ersatz](https://wiki.call-cc.org/eggref/5/ersatz)
    - [lowdown](https://wiki.call-cc.org/eggref/5/lowdown)
    - [scss](https://wiki.call-cc.org/eggref/5/scss)
    - [srfi-1](https://wiki.call-cc.org/eggref/5/srfi-1)
    - [srfi-13](https://wiki.call-cc.org/eggref/5/srfi-13)
    - [srfi-14](https://wiki.call-cc.org/eggref/5/srfi-14)
    - [sxml-transforms](https://wiki.call-cc.org/eggref/5/sxml-transforms)
    - [symbol-utils](https://wiki.call-cc.org/eggref/5/symbol-utils)
    - [utf8](https://wiki.call-cc.org/eggref/5/utf8)
- Git

## Usage

    repo2html <destination> <template-directory>

Run `repo2html` inside the root directory of a Git repository or a bare Git
repository.

- `<destination>` is the path to the directory that you want the HTML files to
  be generated in.
- `<template-directory>` is the path to the directory that contains a
  `default.html` file. Refer to `assets/templates/default.html` for an example
  `default.html` file.

The HTML that's generated represents the state of the `HEAD` commit, not the
current state of the work tree.

## Installation

### For all users, using root access

This section guides you through installing the required dependencies, compiling
a binary, and then installing the binary.  By default, `repo2html` installs
into the `/usr/local/bin` directory.

1. Ensure you're in the `repo2html` git repository.
2. As root, run `make dependencies` to install the required Chicken Scheme eggs.
3. Run `make` to compile a static `repo2html` binary in the current directory.
4. As root, run `make install` to copy the `repo2html` binary into the
   `/usr/local/bin` directory.

### For one user, without root access

If you prefer, you can compile and install without root access.

1. Set the following environment variables appropriately.
   The following will install executables at `~/bin` and chicken libraries at `~/var/lib/chicken`.
   (Verify the location of the `CHICKEN_REPOSITORY_PATH` on your system, as it might not be `/var/lib/chicken/11`):

    ```
    export CHICKEN_INSTALL_PREFIX="$HOME"
    export CHICKEN_REPOSITORY_PATH="$HOME"/var/lib/chicken:/var/lib/chicken/11
    export CHICKEN_INSTALL_REPOSITORY="$HOME"/var/lib/chicken
    ```

2. Ensure you're in the `repo2html` git repository.
3. Run `make dependencies` to install the required Chicken Scheme eggs.
4. Run `make` to compile a static `repo2html` binary in the current directory.
5. Run `make install` to copy the `repo2html` binary into the `~/bin` directory.

<!-- FIXME this part isn't true anymore since we broke the source code up into modules.
**Note**: If you want to use `main.scm` as the `repo2html` executable, instead
of compiling a static binary file, then run
`cp main.scm /usr/local/bin/repo2html` as root.
-->

### Compiling for older systems 

A Dockerfile is included that will build `repo2html` using Debian 11 (bullseye) running glibc-2.31.
The resulting binary should be compatible with systems running both old and newer versions of glibc.

1. Ensure docker is installed (`sudo apt install docker.io`)
2. Run `make compile-on-debian-11`

It takes a while and downloads a lot.

## Templates

You can customize `repo2html` by modifying the template file
`assets/templates/default.html`, and then specifying the directory containing
your modified `default.html` as the second command-line argument when running
`repo2html`.

For example, if you placed a `default.html` template file in
`~/var/templates`, and you serve HTML files from `/var/www/git`, then you run
`repo2html /var/www/git ~/var/templates`.

Templates are rendered using [ersatz](https://wiki.call-cc.org/eggref/5/ersatz),
whose syntax is similar to the [Jinja2](https://palletsprojects.com/p/jinja/)
templating system.

If you use the `include` directive in your modified `default.html` template
file, `repo2html` will look for the templates to be included in that same
specified directory, where your `default.html` template lives.

The following variables are available to templates:

- `source_files_list`: a list of all the files in the repo.
- `repository_description`: a short description of the repo, if provided. See configuration.
- `repository_name`: the name of the repo. See configuration.
- `repository_path`: the URL path from the forge root to the repo.
- `repository_path_parent`: repository_path without the repo itself.
- `repository_ancestors`: a list of each path element in repository_path_parent.
- `readme_file`: the filename of the README file, if any. used for the "about" link.
- `license_file`: the filename of the LICENSE file, if any. used for the "license" link.
- `issues_file`: if any files exist in ISSUES/, this will be set to "ISSUES".
- `repo2html_version`: the blob hash of main.scm in this repository.
- `source_file`: the filename of the file being rendered.
- `root`: always empty. # TODO FIXME
- `elements`: a list of each path element leading to the file being rendered.
- `basename`: the filename being rendered, with one extension removed.
- `extension`: the extension removed in basename.
- `relative_root`: `html/` if the current file is `/index.html` otherwise empty.

## Configuration

`repo2html` understands the following configuration settings.

These settings are typically set using `git config` like

```git config repo2html.<setting> value```

Environment variables may also be used for configuration, and take precedence over settings in `git config`:

```export REPO2HTML_<SETTING>=value```

### Name

- `$REPO2HTML_NAME` or
- `git config repo2html.name`
- default: the repo's directory name

Your repository "name" is, by default, the name of the directory as specified in the first argument provided to `repo2html`.

You may override this if you prefer a more human-friendly name, for example:

`git config repo2html.name "Git Repository To Hypertext Markup Language Generator"`

This affects the template variable `repository_name` but not `repository_path`.

### Description

- `$REPO2HTML_DESCRIPTION` or
- `git config repo2html.description` or
- edit the `.git/description` file
- default: ""

A short description for the repo.
This may also be set by adding a description in a `description` file in the root directory of your Git repository.
(This is the same mechanism that `cgit` uses.)

### ForgeRoot

- `$REPO2HTML_FORGEROOT` or
- `git config repo2html.forgeroot`

This is a helper intended for use when [building a git forge](#creating-a-git-forge-on-your-web-server).

It specifies the filesystem path to the directory containing (possibly a hierarchy of) html output directories.

This affects the template variables `repository_path`, `repository_path_parent`, and `repository_ancestors`, when the repo is found to be in a subdirectory of `ForgeRoot`. 

Example:

With forgeroot unset, the command  
`repo2html /var/www/repos/mike/projecta/repo1 /path/to/templates`  
will set the variable `repository_path` to `repo1`

With forgeroot set to `/var/www/repos`, the command  
`repo2html /var/www/repos/mike/projecta/repo1 /path/to/templates`  
will set the variable `repository_path` to `mike/projecta/repo1`

When your forge is built with a shared git user, it's convenient to use --global to specify this just once, like:

`sudo -u git git config --global repo2html.forgeroot /var/git-repos/`

## Creating a Git forge on your web server

Refer to
[Create a Git forge with repo2html](documentation/create-a-git-forge-with-repo2html.md)
to learn how use `repo2html` in a `post-receive` hook to auto-generate HTML
representations of bare Git repositories on a remote web server after you `git
push` to them.

## Alternatives

### Git Repo Static Page Generators

Other tools like `repo2html`, that generate a set of HTML files representing the contents of a git repository.

- [stagit](https://codemadness.org/git/stagit/file/README.html)
- [depp](https://git.8pit.net/depp.git/)
- [git-arr](https://blitiri.com.ar/p/git-arr/)
